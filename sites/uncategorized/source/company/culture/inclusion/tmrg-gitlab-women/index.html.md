---
layout: markdown_page
title: "TMRG - GitLab Women"
description: "An overview of our remote TMRG GitLab Women"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-women/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction



## Mission

The mission of the GitLab Women TMRG is to cultivate an inclusive environment that supports and encourages those who identify as women to advance their skills and leadership potential through connection, mentorship, collaboration and discussion. This group shall serve as a forum for women to find their voice and be heard within the GitLab community. Through networking, socializing, and professional development, we hope to attract and retain women into GitLab’s positions. This group is open to all members of the GitLab community.

## Leads
* [Kyla Gradin Dahl](https://about.gitlab.com/company/team/#kyla) Lead
* [Samantha Lee](https://about.gitlab.com/company/team/#slee24) Co Lead 


## Executive Sponsors
* [Robin Schulman](https://about.gitlab.com/company/team/#rschulman) - Chief Legal Officer and Corporate Secretary
* [Michael McBride](https://about.gitlab.com/company/team/#mmcb) - Chief Revenue Officer

## How To Get Involved
* Please sign up [here](https://groups.google.com/a/gitlab.com/g/womenstmrg)


## Upcoming Events 
Find upcoming events that we'll be organizing or participating in on our [Women TMRG workboard](https://gitlab.com/gitlab-com/women-tmrg/-/boards/2419883?&label_name[]=Events%20-%20Women%20TMRG). 

Feel free to add a new event using the `Events - Womens TMRG` label and consider using the `event-or-conference-fyi` issue template. 

## Related Performance Indicators and Goals

* [Women at GitLab](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-at-gitlab)
* [GitLab Women in Management - 40% Goal](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-in-management)
* [GitLab Women in Senior Leadership and Executive Roles](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-in-senior-leadership-and-executive-roles)

## Career Development Resources

Women at GitLab looking for career development resources can use this list to explore both opportunities available to the entire GitLab team for career development and helpful resources that have been shared in the [#women Slack channel](https://app.slack.com/client/T02592416/C7V402V8X) 

If you have a resource you'd like to include, please open a merge request and contribute to this page!

| Resource | Description |
| ----- | ----- | 
| [Women at GitLab Mentorship Program](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/) | FY22 Q2-Q3 program opportunity for mentorship at GitLab |
| [GitLab Internship for Learning](/handbook/people-group/learning-and-development/career-development/#internship-for-learning) | If your manager has coverage, you can spend a percentage of your time working (through an 'internship') with another team |
| [Growth and Development benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) | This benefit is available for GitLab team members and can be used to cover professional development costs associated with external resources |
| [CEO Shadow Program](/handbook/ceo/shadow/)| The goal of the CEO Shadow Program is to give current and future directors and senior leaders at GitLab an overview of all aspects of the company |
| [Women in the Workplace Study Webinar](https://www.hracuity.com/webinar/leaninstudy-2020?utm_campaign=Content%20%7C%20Webinar%20%7C%20LeanIn&utm_medium=email&_hsmi=98577248&_hsenc=p2ANqtz-8kbiJWLM1fyvHgX1E15FQtDxjVDla2B4C3p2G7wSvpUQMJizBBdpFhEpmhEdG7PeYR-ekzWWPWhWK9EfpXoZwk5m6dWQ&utm_content=98577248&utm_source=hs_email) | A webinar from Lean In discussing the unique impact that COVID-19 has had on women of different races and ethnicities, working mothers, women in senior leadership and women with disabilities |
| [The Harvard Gazette - Women less inclined to self-promote than men, even for a job](https://news.harvard.edu/gazette/story/2020/02/men-better-than-women-at-self-promotion-on-job-leading-to-inequities/) | Study finds female workers’ deep discomfort over touting skills, experience adds to gender gap in promotions, pay |
| [People Experience Shadow Program](https://about.gitlab.com/handbook/people-group/people-experience-shadow-program/)| The program allows for anyone to get an understanding of the People Experience responsibilities and allows for collaboration and iteration. |
| [Stanford Continuing Studies Department](https://continuingstudies.stanford.edu/) | Standford offers many continuing education programs in multiple diciplines including leadership, writing, and professional/personal development |

### LinkedIn Learning Pathways

Review two learning pathways built by the Professional Member Development subcommittee shared in FY21 Q1 and Q2 as[LinkedIn Learning cohorts](/handbook/people-group/learning-and-development/linkedin-learning/#linkedin-learning-cohorts). You can access these courses and complete them on your own time using GitLab Learn.

1. [GitLab Women in Leadership Course](https://gitlab.edcast.com/pathways/women-in-leadership)
1. [Mastering Self-Motivation and Self-Advocacy Course](https://gitlab.edcast.com/pathways/mastering-self-motivation-and-self-aadvocacy)

**FY22 Q1 LinkedIn Learning Cohort Discussion**

<iframe width="560" height="315" src="https://www.youtube.com/embed/PX7w1D1-nmQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Suggested LinkedIn Learning Courses

GitLab team members can obtain a [LinkedIn Learning license](/handbook/people-group/learning-and-development/linkedin-learning/) that includes access to more than 8,000 courses. The following courses have been selected by members of the women's TMRG as courses that members might benefit from in both their career or personal development.

If you've completed a LinkedIn Learning course that you found beneficial, please add it to this resource!

#### Professional Development Focus

1. [Women in Leadership Learning Path](https://www.linkedin.com/learning/paths/women-in-leadership-3?u=2255073)
1. [Leadership Strategies for Women](https://www.linkedin.com/learning/leadership-strategies-for-women/women-lead-differently?u=2255073)
1. [Become a Courageous Female Leader](https://www.linkedin.com/learning/become-a-courageous-female-leader/joan-kuhl-s-courageous-leadership-course?u=2255073)
1. [Problem Solving Skills](https://www.linkedin.com/learning/paths/develop-critical-thinking-decision-making-and-problem-solving-skills?u=2255073)
1. [Engage Meaningfully in Allyship and Anti-Racism](https://www.linkedin.com/learning/paths/how-to-engage-meaningfully-in-allyship-and-anti-racism?u=2255073)
1. [Communication Tips](https://www.linkedin.com/learning/communication-tips/welcome-to-the-series?u=2255073)
1. [Customer Service Foundation](https://www.linkedin.com/learning/customer-service-foundations-2)
1. [Develop Your Customer Services Skills Path](https://www.linkedin.com/learning/paths/develop-your-customer-service-skills)

#### Personal Development Focus

1. [Life Mastery - Achieving happiness and success](https://www.linkedin.com/learning/life-mastery-achieving-happiness-and-success/welcome?u=2255073)
1. [Mindful Working](https://www.linkedin.com/learning/mindful-working-11-ways-to-improve-how-you-work/introduction?u=2255073)
1. [Creativity for All](https://www.linkedin.com/learning/creativity-for-all-weekly/what-is-creative-cross-training?u=2255073)
1. [Leading Yourself](https://www.linkedin.com/learning/leading-yourself-2017/welcome?u=2255073)
1. [Mastering Self-Motivation](https://www.linkedin.com/learning/mastering-self-motivation/self-motivation-your-driving-force?u=2255073)
1. [Being Your Own Fierce Self-Advocate](https://www.linkedin.com/learning/being-your-own-fierce-self-advocate/standing-up-for-yourself-every-day?u=2255073)


## Book Suggestions

The following are books suggested by GitLab team members in the [#women Slack channel](https://app.slack.com/client/T02592416/C7V402V8X). Please open an merge request to add books to this page, and consider including a short review of the book from your perspective! 

| Book Title and Author | Optional Book Review |
| ----- | ----- |
| [WolfPack by Abby Wambach](http://abbywambach.com/books/wolfpack/) | |
| [Radical Candor by Kim Scott](https://www.radicalcandor.com/the-book/) | |
| [Lean In by Sheryl Sandberg](https://leanin.org/book) | |
| [The Memo by Minda Harts](https://www.mindaharts.com/book/) | |
| [Burnout - The Secret to Unlocking the Stress Cycle by Amelia Nagoski and Emily Nagoski](https://bookshop.org/books/burnout-the-secret-to-unlocking-the-stress-cycle/9781984818324) | |


## Additional Resources

### Women TMRG logo 
Download the [GitLab Women TMRG logo here](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/design/gitlab-brand-files/gitlab-logo-files/global-diversity-inclusion/full-color/png/di-women-logo-rgb.png).
